#!/usr/bin/python

# Author: Lincong Li
# Email: lli@phytec.com
# Date: March/10/2016

import subprocess
from Tkinter import *
from tkdnd_wrapper import TkDND
import tkMessageBox
import os
import csv

# '/Users/lincongli/Desktop/Phytec_Intern/result_analysis_project/tkdnd/tkdnd2.8'
tkdnd2_8_dir = os.getcwd() + '/tkdnd/tkdnd2.8'
os.environ['TKDND_LIBRARY'] = tkdnd2_8_dir

# shared global variables
PLOT_TYPE_EVENT_TEMP = 'event_and_temperature_plot'
PLOT_TYPE_TEMP_RESULTS = 'thermal_results_plot'

# plot_info_lookup_table contains column information for each plot
plot_info_lookup_table = {PLOT_TYPE_EVENT_TEMP: \
                              ['name/failure_count/start_time/start_count/detail/success_count/end_time/write_speed/read_speed/result',
                               'TEXT/INT/TEXT/INT/TEXT/INT/TEXT/INT/INT/INT'],
                          PLOT_TYPE_TEMP_RESULTS: \
                              ['date_time/command/probe_temperature/chamber_temperature/junction_temperature/current',
                               'TEXT/TEXT/REAL/REAL/REAL/REAL'] # to be implemented
                          }

# thermal testing commands lookup table
thermal_commands_lookup = {'\'(while true; do dhrystone 50000000; done) >/dev/null 2>&1 &\'': 'dhrystone;', \
                           '\'cpuloadgen 100 >/dev/null 2>&1 & /usr/bin/SGX/demos/Raw/OGLES2ChameleonMan >/dev/null 2>&1 &\'' : 'cpuloadgen & SGX/demos', \
                           '\'\'': 'idle'}

'''
Global variables for plot of FCC test
'''

# if a file is bigger than MAX_PLOT_SIZE bytes, we only plot the last part of
#  the file that is about MAX_PLOT_SIZE bytes
MAX_PLOT_SIZE = 150000 # 150KB
# size of each row
ROW_SIZE = 98
# size of the non-row parts of the file, such as the size of the metadata
FILE_SIZE_OFFSET = 18378
MAX_PLOT_ROW = (MAX_PLOT_SIZE - FILE_SIZE_OFFSET) / ROW_SIZE

# start the client program which does the actually plotting job
plotter_proc_list = []
browse_selected_file = None
option_Menu_selected_file = None
dnd_selected_file = None
first_time_browse_select = False
fn_from_dnd = False
option_plot_FCC = False
start_plotter_count = 0
selected_file_count = 0

'''
Global variables for plot of thermal test
'''
option_plot_Thermal = False
FCC_proc_list = []
thermal_proc_list = []

def check_csv_format(fn, file_format):
    with open(fn, 'rb') as fin:  # `with` statement available in 2.5+
        dr = csv.DictReader(fin)  # comma is default delimiter
        try:
            first_row = next(dr)
        except:
            tkMessageBox.showinfo("Error Message", "Make sure the file is in the right .csv format")
            return False
        expected_column_entries = plot_info_lookup_table[file_format][0].split('/')
        # print "titles are: ", first_row
        # print first_row.keys()
        for entry in first_row.keys():
            if entry not in expected_column_entries:
                tkMessageBox.showinfo("Error Message", "Selected .csv file doesn't have correct column entries")
                return False
        return True


def reset_global_states(mode):
    if mode == PLOT_TYPE_EVENT_TEMP:
        global browse_selected_file, option_Menu_selected_file, dnd_selected_file
        global first_time_browse_select, fn_from_dnd, option_plot_FCC, start_plotter_count
        global selected_file_count
        browse_selected_file = None
        dnd_selected_file = None
        first_time_browse_select = False
        fn_from_dnd = False
        start_plotter_count = 0
        selected_file_count = 0


def raise_frame(frame):
    global option_plot_FCC, option_plot_Thermal
    # if we are going to the FCC frame
    if frame == FCC_frame:
        option_plot_FCC = True
        dropdown_window(FCC_frame)

    # if we are going to the thermal frame
    elif frame == thermal_frame:
        option_plot_Thermal = True
        dropdown_window(thermal_frame)

    elif frame == main_frame:
        # dropdown_window(FCC_frame) # re-initialize the dropdown window
        # dropdown_window(thermal_frame) # re-initialize the dropdown window
        reset_global_states(PLOT_TYPE_EVENT_TEMP) # reset all global states
        option_plot_FCC = False
        option_plot_Thermal = False
    else:
        option_plot_FCC = False
        option_plot_Thermal = False
    frame.tkraise()


def plot_FCC():
    global browse_selected_file, option_Menu_selected_file, dnd_selected_file, fn_from_dnd

    if browse_selected_file is None and option_Menu_selected_file.get() == '-select file-' \
            and fn_from_dnd is False:
        tkMessageBox.showinfo("Error Message", "Select a file first please")
        return

    global start_plotter_count, selected_file_count, dnd_selected_file
    if fn_from_dnd:
        plot_file = dnd_selected_file
        fn_from_dnd = False

    elif selected_file_count is start_plotter_count and first_time_browse_select is True:
        plot_file = browse_selected_file
    else:
        plot_file = option_Menu_selected_file.get()
        # use the file name to find the full path
        file_list = find_all_files_end_with('.csv')
        for file_path in file_list:
            if plot_file == file_path.split('/')[-1]:
                plot_file = file_path
                break


    # print 'Start button pressed, selected file is: ', file_path
    print plot_file
    if check_csv_format(plot_file, PLOT_TYPE_EVENT_TEMP):
        plotter_proc = subprocess.Popen(['./plotter.py', plot_file, PLOT_TYPE_EVENT_TEMP, str(len(plotter_proc_list)+1)], )
        plotter_proc_list.append(plotter_proc)

    start_plotter_count += 1


def start_plotter():  # function
    global browse_selected_file, option_Menu_selected_file, dnd_selected_file, fn_from_dnd

    # check if there is any file selected from either the dropdown menu or the browse button
    if browse_selected_file is None and option_Menu_selected_file.get() == '-select file-' \
            and fn_from_dnd is False:
        tkMessageBox.showinfo("Error Message", "Select a file first please")
        return

    # determine where is the file source
    global start_plotter_count, selected_file_count, dnd_selected_file
    if fn_from_dnd:
        plot_file = dnd_selected_file
        fn_from_dnd = False

    elif selected_file_count is start_plotter_count and first_time_browse_select is True:
        plot_file = browse_selected_file
    else:
        plot_file = option_Menu_selected_file.get()
        # use the file name to find the full path
        file_list = find_all_files_end_with('.csv')
        for file_path in file_list:
            if plot_file == file_path.split('/')[-1]:
                plot_file = file_path
                break

    # print 'Start button pressed, selected file is: ', file_path
    print plot_file
    if option_plot_FCC:
        print 'Plot FCC'
        if check_csv_format(plot_file, PLOT_TYPE_EVENT_TEMP):
            global FCC_proc_list
            plotter_proc = subprocess.Popen(['./plotter.py', plot_file, PLOT_TYPE_EVENT_TEMP, str(len(plotter_proc_list)+1)], )
            # plotter_proc_list.append(plotter_proc)
            FCC_proc_list.append(plotter_proc)
        start_plotter_count += 1

    elif option_plot_Thermal:
        print 'Plot Thermal'
        if check_csv_format(plot_file, PLOT_TYPE_TEMP_RESULTS):
            global thermal_proc_list
            plotter_proc = subprocess.Popen(['./plotter.py', plot_file, PLOT_TYPE_TEMP_RESULTS, str(len(plotter_proc_list)+1)], )
            # plotter_proc_list.append(plotter_proc)
            thermal_proc_list.append(plotter_proc)
        start_plotter_count += 1

def quit_plotter(type):
    if type == PLOT_TYPE_TEMP_RESULTS:
        global thermal_proc_list
        proc_num = len(thermal_proc_list)
        if proc_num is not 0:
            proc = thermal_proc_list[proc_num - 1]
            thermal_proc_list.remove(proc)
            proc.kill()
            return len(thermal_proc_list) is 0 # return true if there is no more plotting process
        else:
            return True

    elif type == PLOT_TYPE_EVENT_TEMP:
        global FCC_proc_list
        proc_num = len(FCC_proc_list)
        if proc_num is not 0:
            proc = FCC_proc_list[proc_num - 1]
            FCC_proc_list.remove(proc)
            proc.kill()
            return len(FCC_proc_list) is 0 # return true if there is no more plotting process
        else:
            return True


def quit_all(master):
    while quit_plotter(PLOT_TYPE_TEMP_RESULTS) is not True:
        continue

    while quit_plotter(PLOT_TYPE_EVENT_TEMP) is not True:
        continue
    master.quit()


def find_all_files_end_with(suffix):
    file_paths = []
    for root, dirs, files in os.walk("./"):
        for file in files:
            if file.endswith(suffix):
                # print(os.path.join(root, file))
                file_paths.append(os.path.join(root, file))
    return file_paths


def browse():
    global selected_file_count
    global start_plotter_count
    global browse_selected_file
    global first_time_browse_select
    global fn_from_dnd
    fn_from_dnd = False
    first_time_browse_select = True
    from tkFileDialog import askopenfilename
    Tk().withdraw()
    filename = askopenfilename()
    print filename

    selected_file_count = start_plotter_count
    browse_selected_file = filename


def dropdown_window(master):
    global option_Menu_selected_file
    global file_selected_from_browse
    global file_selected_from_menu
    sel_result = StringVar(master)
    sel_result.set("-select file-")  # initial value
    file_list = find_all_files_end_with('.csv')
    options = []
    # get rid of the path fomrat and save only the file name
    for file_path in file_list:
        fn = file_path.split('/')[-1]
        options.append(fn)
    option = apply(OptionMenu, (master, sel_result) + tuple(options))
    option.pack()
    option.place(relx=0.2, rely=0.1)
    browse_button = Button(master, text="Browse", command=browse)
    browse_button.pack()
    browse_button.place(relx=0.7, rely=0.1)

    file_selected_from_menu = True
    file_selected_from_browse = False
    option_Menu_selected_file = sel_result


def init_main_frame(master):
    main_frame = Frame(master, width=300, height=300, bd=5)
    main_frame.pack()
    go_FCC_button = Button(main_frame, text="plot FCC test results", command=lambda: raise_frame(FCC_frame))
    go_FCC_button.pack()
    go_FCC_button.place(relx=0.25, rely=0.2)
    # make this button to be able to handle dnd for the FCC test results plot
    dnd.bindtarget(go_FCC_button, FCC_dnd_handler, 'text/uri-list')

    go_thermal_button = Button(main_frame, text="plot Thermal test results", command=lambda: raise_frame(thermal_frame))
    go_thermal_button.pack()
    go_thermal_button.place(relx=0.2, rely=0.5)

    # make this button to be able to handle dnd for the thermal test results plot
    dnd.bindtarget(go_thermal_button, Thermal_dnd_handler, 'text/uri-list')

    quit_button = Button(main_frame, text="Quit", command=lambda: quit_all(master))
    quit_button.pack()
    quit_button.place(relx=0.4, rely=0.9)

    return main_frame


def init_FCC_frame(master):
    FCC_frame = Frame(master, width=300, height=300, bd=5)
    FCC_frame.pack()
    # initialize the dropdown window
    dropdown_window(FCC_frame)
    # initialize the "start plotting" button
    button = Button(FCC_frame, text='Start plotting', command=lambda: start_plotter())
    button.pack()
    button.place(relx=0.3, rely=0.3)
    # initialize the "quit plotting" button
    quit_button = Button(FCC_frame, text="Quit plotting", command=lambda: quit_plotter(PLOT_TYPE_EVENT_TEMP))
    quit_button.pack(pady=50, padx=50)
    quit_button.place(relx=0.3, rely=0.5)

    # return to the main menu button
    return_button = Button(FCC_frame, text='<<', command=lambda: raise_frame(main_frame))
    return_button.pack(pady=50, padx=50)
    return_button.place(relx=0.3, rely=0.9)
    return FCC_frame


def init_thermal_test_frame(master):
    thermal_frame = Frame(master, width=300, height=300, bd=5)
    thermal_frame.pack()

    # initialize the dropdown window
    dropdown_window(thermal_frame)
    # initialize the "start plotting" button
    button = Button(thermal_frame, text='Start plotting', command=lambda: start_plotter())
    button.pack()
    button.place(relx=0.3, rely=0.3)
    # initialize the "quit plotting" button
    quit_button = Button(thermal_frame, text="Quit plotting", command=lambda: quit_plotter(PLOT_TYPE_TEMP_RESULTS))
    quit_button.pack(pady=50, padx=50)
    quit_button.place(relx=0.3, rely=0.5)

    # return to the main menu button
    return_button = Button(thermal_frame, text='<<', command=lambda: raise_frame(main_frame))
    return_button.pack(pady=50, padx=50)
    return_button.place(relx=0.3, rely=0.9)
    return thermal_frame


def FCC_dnd_handler(event):
    global fn_from_dnd, dnd_selected_file, option_plot_FCC, option_plot_Thermal
    file_path = event.data
    # print file_path
    fn_from_dnd = True
    dnd_selected_file = file_path
    if check_csv_format(dnd_selected_file, PLOT_TYPE_EVENT_TEMP):
        option_plot_FCC = True
        option_plot_Thermal = False
        start_plotter()


def Thermal_dnd_handler(event):
    global fn_from_dnd, dnd_selected_file, option_plot_Thermal, option_plot_FCC
    file_path = event.data
    # print file_path
    fn_from_dnd = True
    dnd_selected_file = file_path
    if check_csv_format(dnd_selected_file, PLOT_TYPE_TEMP_RESULTS):
        option_plot_Thermal = True
        option_plot_FCC = False
        start_plotter()


if __name__ == '__main__':
    root = Tk()  # main window
    dnd = TkDND(root)
    thermal_frame = init_thermal_test_frame(root)

    FCC_frame = init_FCC_frame(root)

    main_frame = init_main_frame(root)

    for frame in (main_frame, FCC_frame, thermal_frame):
        frame.grid(row=0, column=0, sticky='news')

    raise_frame(main_frame)
    root.mainloop()
