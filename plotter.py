#!/usr/bin/python

# Author: Lincong Li
# Email: lli@phytec.com
# Date: March/10/2016

import sqlite3, sys, csv, unicodedata
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec
from Qt_controller import PLOT_TYPE_EVENT_TEMP, PLOT_TYPE_TEMP_RESULTS
from Qt_controller import plot_info_lookup_table, thermal_commands_lookup
from Qt_controller import MAX_PLOT_ROW, MAX_PLOT_SIZE
from datetime import datetime
import time
import os
import pprint

'''
create a sql table containing information (name, write_speed, read_speed, time) for the read/write speed plotting
'''


def read_write_info_into_DB(fn):
    with open(fn, 'rb') as fin:  # `with` statement available in 2.5+
        # csv.DictReader uses first line in file for column headings by default
        dr = csv.DictReader(fin)  # comma is default delimiter
        first_log = next(dr)
        while first_log['start_time'] == '':
            first_log = next(dr)
        start_time = first_log['start_time']  # reference time
        create_table_cmd = 'CREATE TABLE rw_table (name TEXT, end_time TEXT, write_speed INT, read_speed INT);'
        insert_table_cmd = 'INSERT INTO rw_table (name, end_time, write_speed, read_speed) VALUES (?, ?, ?, ?);'
        to_db = []
        for i in dr:
            one_row = (i['name'], i['end_time'], i['write_speed'], i['read_speed'])  # add to the tuple
            to_db.append(one_row)  # add the tuple to the list

        # create the table to store the csv file data
        con = sqlite3.connect(":memory:")
        cur = con.cursor()
        cur.execute(create_table_cmd)
        cur.executemany(insert_table_cmd, to_db)
        con.commit()
        return cur, float(start_time)


'''
Read a file with name "fn" into a database file and make a table to store the content of the
csv file according to the type of plotting the user wants to do
'''


def read_entire_csv_into_DB(fn, plot_type):
    with open(fn, 'rb') as fin:  # `with` statement available in 2.5+
        # csv.DictReader uses first line in file for column headings by default
        dr = csv.DictReader(fin)  # comma is default delimiter
        first_log = next(dr)

        # Extracting the reference time from csv file (different csv formats have different ways to extract the time information)
        if plot_type == PLOT_TYPE_EVENT_TEMP:
            # skip all empty rows, if the 'start_time' entry is empty, assume the entire row is empty
            while first_log['start_time'] == '':
                try:
                    first_log = next(dr)
                except StopIteration:
                    print 'ERROR: File has no content'
                    exit(-1)

            start_time = first_log['start_time']  # reference time

        elif plot_type == PLOT_TYPE_TEMP_RESULTS:
            first_log = next(dr)
            start_time = first_log['date_time']  # reference time

        try:  # make sure the plot type is valid
            attribute_name_list = plot_info_lookup_table[plot_type][0].split('/')
            attribute_type_list = plot_info_lookup_table[plot_type][1].split('/')
        except:
            print 'Error: Unknown plot type %s', plot_type
            exit(-1)

        to_db_all = []
        to_db = []
        for i in dr:
            one_row = ()
            for entry in attribute_name_list:
                one_row += (i[entry],)  # add to the tuple
            to_db_all.append(one_row)  # add the tuple to the list

        file_stat = os.stat(fn)
        file_size = file_stat.st_size
        total_row_num = len(to_db_all) - 1
        # check file size, only applies to PLOT_TYPE_EVENT_TEMP
        if MAX_PLOT_SIZE < file_size and plot_type == PLOT_TYPE_EVENT_TEMP:
            for i in range(total_row_num - MAX_PLOT_ROW, total_row_num):
                to_db.append(to_db_all[i])
        else:  # always use all data from the thermal tests results plot
            to_db = to_db_all

    # create the table to store the csv file data
    con = sqlite3.connect(":memory:")
    cur = con.cursor()

    # create the sql commands to create the table and insert content into the table
    sql_create_table_cmd = 'CREATE TABLE res ('
    sql_insert_table_cmd = 'INSERT INTO res ('
    if len(attribute_type_list) is not len(attribute_name_list):
        print 'Error: check plot_info_lookup_table and make len of attribute_type_list is the same as that of attribute_name_list'
        exit(-1)

    sql_create_table_cmd += (attribute_name_list[0] + ' ')
    sql_create_table_cmd += attribute_type_list[0]
    sql_insert_table_cmd += attribute_name_list[0]
    for i in range(1, len(attribute_type_list)):
        sql_create_table_cmd += (', ' + attribute_name_list[i] + ' ')
        sql_create_table_cmd += attribute_type_list[i]

        sql_insert_table_cmd += (', ' + attribute_name_list[i])

    sql_create_table_cmd += ');'

    sql_insert_table_cmd += ') VALUES (?'
    for i in range(len(attribute_type_list) - 1):
        sql_insert_table_cmd += ', ?'
    sql_insert_table_cmd += ');'

    # eg:
    # CREATE TABLE res (name TEXT, failure_count INT, start_time TEXT, start_count INT, detail TEXT, success_count INT, end_time TEXT);
    # INSERT INTO res (name, failure_count, start_time, start_count, detail, success_count, end_time) VALUES (?, ?, ?, ?, ?, ?, ?);

    # execute the commands
    print 'sql_create_table_cmd is: ', sql_create_table_cmd
    print 'sql_insert_table_cmd is: ', sql_insert_table_cmd
    cur.execute(sql_create_table_cmd)
    cur.executemany(sql_insert_table_cmd, to_db)

    con.commit()
    return cur, start_time


def convert_str_list_2_float_list(str_temp_list):
    temp_list = []
    for i in str_temp_list:
        val_unicode = i[0]
        val_string = unicodedata.normalize('NFKD', val_unicode).encode('ascii', 'ignore')
        if '.' in val_string:
            f = float(val_string)
            temp_list.append(f)
    return temp_list


def unicode_2_float(unicode_str):
    string_val = start_time_str = unicodedata.normalize('NFKD', unicode_str).encode('ascii', 'ignore')
    if '.' in string_val:
        float_val = float(string_val)
    else:
        float_val = 0.0
    return float_val


def plot_time_seg(seg_list):
    start_time = seg_list[0][0]
    end_time = seg_list[-1][1]
    t = np.arange(start_time, end_time, 0.00001)
    index_time = start_time
    seg_res_list = []
    for period in seg_list:
        period_start = period[0]
        period_end = period[1]
        while index_time < period_start:
            index_time += 0.00001
            seg_res_list.append(0)

        while index_time <= period_end:
            index_time += 0.00001
            seg_res_list.append(1)

    plt.plot(t, seg_res_list, 'r--')
    plt.axis([start_time, end_time, 0, 3])
    plt.fill_between(t, seg_res_list)
    plt.show()


'''
rearrange the results from the query into a list of list (2 elements: start time and end time)
return the rearranged list
option:
    1: both values minus start time
    2: only the 1st value minus start time
    3: only the 2nd value minus start time
    4: neither side minus start time
'''


def rearrange_query_res(res, start_time, option=1):
    time_list = []
    start_time = float(start_time)
    if option == 1:
        for i in res:
            time_list.append([unicode_2_float(i[0]) - start_time, unicode_2_float(i[1]) - start_time])
    elif option == 2:
        for i in res:
            time_list.append([unicode_2_float(i[0]) - start_time, unicode_2_float(i[1])])
    elif option == 3:
        for i in res:
            time_list.append([unicode_2_float(i[0]), unicode_2_float(i[1]) - start_time])
    elif option == 4:
        for i in res:
            time_list.append([unicode_2_float(i[0]), unicode_2_float(i[1])])
    else:
        print 'Error in rearrange_query_res(). Invalid option argument'
        exit(-1)

    return time_list


def get_time_list(cur, module, start_time):
    cur.execute("SELECT start_time, end_time FROM res WHERE name = '%s'" % module)
    res = cur.fetchall()
    return rearrange_query_res(res, start_time)


'''
Return a list of pass/fail history of the module
'''


def get_module_history(cur, name):
    cur.execute("SELECT failure_count, start_count FROM res WHERE name = '%s'" % name)
    res = cur.fetchall()
    history_list = []
    for i, state in enumerate(res):
        if i is 0:
            history_list.append(1)
        elif state[0] - res[i - 1][0] is 1:  # failure count is increased by 1
            history_list.append(0)
        else:
            history_list.append(1)

    return history_list


'''
Return a string formated in "success_count/total_count"
'''


def get_module_final_res(cur, name):
    cur.execute("SELECT MAX(start_count) FROM res WHERE name = '%s'" % name)
    res = cur.fetchall()
    total_test_count = res[0][0]
    cur.execute(
        "SELECT success_count FROM res WHERE name = '{0}' and start_count = '{1}'".format(name, total_test_count))
    res = cur.fetchall()
    success_test_count = res[0][0]
    # print 'total_test_count: ', total_test_count
    # print 'success_test_count: ', success_test_count
    test_result = str(success_test_count) + '/' + str(total_test_count)
    return test_result


'''
Return a list of dictionaries. Each dictionary contains the following information:
    1. name of the module
    2. final result (success_count/total_count) of the module
    3. history (a list of "1"s and "0"s showing which tests passed and which ones didn't)
'''


def get_module_info_list(cur):
    # ordered alphabetically ignoring cases
    cur.execute("SELECT DISTINCT name FROM res ORDER BY name COLLATE NOCASE DESC")
    name_list = cur.fetchall()
    res_list = []
    for element in name_list:
        name = unicodedata.normalize('NFKD', element[0]).encode('ascii', 'ignore')
        # for each name, get the final result success/total
        test_result = get_module_final_res(cur, name)
        test_history = get_module_history(cur, name)
        module_dict = {'name': name, 'result': test_result, 'history': test_history}
        res_list.append(module_dict)
    return res_list


'''
Plot temperature during the FCC/CE test
'''


def plot_temperature(plt, cur, start_time):
    cur.execute("SELECT end_time, detail FROM res WHERE name = 'Temperature'")
    res = cur.fetchall()
    x_y_list = rearrange_query_res(res, start_time, 2)
    x = []
    y = []
    for i, element in enumerate(x_y_list):
        if i > 0:
            x.append(element[0])
            y.append(element[1])
    plt.plot(x, y)


def get_spaced_colors(n):
    max_value = 16581375  # 255**3
    interval = int(max_value / n)
    colors = [hex(I)[2:].zfill(6) for I in range(0, max_value, interval)]
    tuple_list = [(int(i[:2], 16), int(i[2:4], 16), int(i[4:], 16)) for i in colors]
    res_list = []
    for tuple in tuple_list:
        color_list = []
        for num in tuple:
            num /= 256.0
            color_list.append(num)
        res_list.append(color_list)
    return res_list


def avg(a, b):
    return (a + b) / 2.0


'''
convert a string in the datetime-formated time to second in integer
'''


def convert_date_time_2_secs(time_stamp, pattern):
    d = datetime.strptime(time_stamp, pattern)
    sec = int(time.mktime(d.timetuple()))
    return sec


'''
convert the datatime-formated time (string list) to seconds (int list)
'''


def convert_date_time_list_2_secs_list(original_formated_time_list, pattern):
    d = datetime.strptime(original_formated_time_list[0], pattern)
    start_time = int(time.mktime(d.timetuple()))
    sec_list = []
    for time_stamp in original_formated_time_list:
        sec = convert_date_time_2_secs(time_stamp, pattern) - start_time
        sec_list.append(sec)
    return sec_list, start_time


'''
returns a dictionary of dictionaries, and each dictionary stands for the time period of running a specific command
'''


def find_cmd_period(fn, cmd_name_list, pattern, start_time):
    res = {}
    # initialize the data structure to store the results
    for i in cmd_name_list:
        res[i] = {}
        res[i]['time'] = []
        res[i]['current'] = []
        res[i]['probe_temperature'] = []
        res[i]['junction_temperature'] = []

    with open(fn, 'rb') as fin:  # `with` statement available in 2.5+
        # csv.DictReader uses first line in file for column headings by default
        dr = csv.DictReader(fin)  # comma is default delimiter
        for i in dr:
            cmd = i['command']
            # cmd = unicodedata.normalize('NFKD', cmd).encode('ascii', 'ignore')
            dt = i['date_time']
            current = i['current']
            P_temp = i['probe_temperature']
            J_temp = i['junction_temperature']
            sec = convert_date_time_2_secs(dt, pattern) - start_time
            if sec >= 0:
                res[cmd]['time'].append(sec)
                res[cmd]['current'].append(current)
                res[cmd]['probe_temperature'].append(P_temp)
                res[cmd]['junction_temperature'].append(J_temp)
    return res


# 13 colors
# red: [1, 0, 0] for indicating errors
color_array = get_spaced_colors(100)

'''
Plot the write and read speed of each module over time. Each figure has 3 subplots and each subplot represents one module.
If there are 9 modules have w/r speed (or only write/read), there would ne 3 figures, and each one has 3 subplots
'''


def plot_FCC_rw_speed(fn, cur):
    module_dict = {}  # a data structure containing the information of which property does each module have (w/r? only r? only w?)
    modules_with_read_write_speed = []
    modules_with_only_read_speed = []
    modules_with_only_write_speed = []
    cur2, start_time = read_write_info_into_DB(fn)  # create a sql table with required columns instead of all columns

    # find all names of modules which did both read and write operations during the test
    cur2.execute("SELECT distinct name FROM rw_table WHERE read_speed is not '' AND write_speed is not ''")
    res = cur2.fetchall()
    for name in res:
        name = unicodedata.normalize('NFKD', name[0]).encode('ascii', 'ignore')
        modules_with_read_write_speed.append(name)
        module_dict[name] = {'write': True, 'read': True}

    # find all names of modules which did read operations during the test
    cur2.execute("SELECT distinct name FROM rw_table WHERE read_speed is NOT ''")
    res = cur2.fetchall()
    for name in res:
        name = unicodedata.normalize('NFKD', name[0]).encode('ascii', 'ignore')
        if name not in modules_with_read_write_speed:
            modules_with_only_read_speed.append(name)
            module_dict[name] = {'write': False, 'read': True}

    # find all names of modules which did write operations during the test
    cur2.execute("SELECT distinct name FROM rw_table WHERE write_speed is NOT ''")
    res = cur2.fetchall()
    for name in res:
        name = unicodedata.normalize('NFKD', name[0]).encode('ascii', 'ignore')
        if name not in modules_with_read_write_speed:
            modules_with_only_write_speed.append(name)
            module_dict[name] = {'write': True, 'read': False}

    # plot basing on the "module_dict"
    subplot_count = 1
    for each_module in module_dict:
        subplot_num = (subplot_count - 1) % 3
        if subplot_num is 0:  # time to make a new figure
            figure_title = 'Figure ' + str(subplot_count) + '  Read/write speed'
            plt.figure(figure_title)
        ax = plt.subplot2grid((3, 1), (subplot_num, 0))

        time = []
        r_speed = []
        # plot read speed over time of "each_module"
        if module_dict[each_module]['read'] is True:
            cur2.execute("SELECT read_speed, end_time FROM rw_table WHERE name is '%s'" % each_module)
            res = cur2.fetchall()
            for tuple in res:
                t = unicodedata.normalize('NFKD', tuple[1]).encode('ascii', 'ignore')
                if '.' in t and type(tuple[0]) is int:
                    t = float(t) - start_time
                    time.append(t)
                    r_speed.append(tuple[0])
            plt.plot(time, r_speed, label='read')

        time = []
        w_speed = []
        # plot write speed over time of "each_module"
        if module_dict[each_module]['write'] is True:
            cur2.execute("SELECT write_speed, end_time FROM rw_table WHERE name is '%s'" % each_module)
            res = cur2.fetchall()
            for tuple in res:
                t = unicodedata.normalize('NFKD', tuple[1]).encode('ascii', 'ignore')
                if '.' in t and type(tuple[0]) is int:
                    t = float(t) - start_time
                    time.append(t)
                    w_speed.append(tuple[0])
            plt.plot(time, w_speed, label='write')
        x_label = 'time (s)  ' + each_module
        ax.set_xlabel(x_label)
        ax.set_ylabel('Speed (Bytes/S)')
        ax.grid(True)
        plt.legend()
        subplot_count += 1

'''
Plot the FCC/CE tests results
'''


def plot_FCC(fn, figure_title):
    cur, start_time = read_entire_csv_into_DB(fn, PLOT_TYPE_EVENT_TEMP)
    module_info_list = get_module_info_list(cur)

    event_plot_data = []
    for module in module_info_list:
        event_plot_data.append(get_time_list(cur, module['name'], start_time))

    plt.figure(figure_title)
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    ax1 = plt.subplot(gs[0])
    ax1.axes.get_yaxis().set_visible(False)
    coordinate_list = []
    x_list = []
    y_scale_factor = 3
    for y, row in enumerate(event_plot_data):
        first_plot = True
        for x, col in enumerate(row):
            x1 = col
            y1 = np.array([y, y]) * y_scale_factor
            y2 = y1 + y_scale_factor
            if module_info_list[y]['history'][x] == 1:
                plt.fill_between(x1, y1, y2=y2, color=color_array[y])
            else:
                plt.fill_between(x1, y1, y2=y2, color=[1, 0, 0])  # plot a red area to indicate that test fails

            if first_plot:
                first_plot = False
                coordinate_list.append([y1[0], y2[0], y])
                x_list.append(x1[0])

        plt.text(avg(x1[1], x1[1] + 6), avg(y1[0], y2[0]), module_info_list[y]['result'],
                 horizontalalignment='center',
                 verticalalignment='center')

    # find the smallest x in the coordinate_list, and use the smallest x as the start point of labels
    x_start = min(x_list)
    for i in coordinate_list:
        plt.text(avg(x_start, x_start - 3), avg(i[0], i[1]), module_info_list[i[2]]['name'], verticalalignment='center',
                 horizontalalignment='center')

    plt.ylim(0, len(event_plot_data) * y_scale_factor)

    ax2 = plt.subplot(gs[1], sharex=ax1)
    plot_temperature(plt, cur, start_time)
    ax2.set_xlabel('time (s)')
    ax2.set_ylabel('Temperature' + u'\N{DEGREE SIGN}')
    ax2.grid(True)

    plot_FCC_rw_speed(fn, cur)
    plt.show()


'''
Plot the thermal tests results
'''


def plot_thermal(fn, figure_title):
    # plt.figure(figure_title)
    # assume the each row is logged every 21 seconds for now
    cur, start_time = read_entire_csv_into_DB(fn, PLOT_TYPE_TEMP_RESULTS)

    # get "probe_temperature" and "chamber_temperature" columns
    cur.execute("SELECT probe_temperature, junction_temperature, current, date_time FROM res")
    res = cur.fetchall()
    raw_format_time = []
    probe_temperature = []
    junction_temperature = []
    current = []
    # put sql results in lists for plotting purpose later
    for i in res:
        probe_temperature.append(i[0])
        junction_temperature.append(i[1])
        current.append(i[2])
        raw_format_time.append(unicodedata.normalize('NFKD', i[3]).encode('ascii', 'ignore'))
    # get time stamps in secs to be used as horizontal axis later
    date_time_pattern = '%a %b %d %H:%M:%S %Y'
    time_stamps, start_time = convert_date_time_list_2_secs_list(raw_format_time,
                                                                 date_time_pattern)  # Thu Mar 03 11:03:10 2016

    # get a list of commands that are used during the test
    cur.execute("SELECT DISTINCT command FROM res")
    res = cur.fetchall()
    cmd_name_list = []
    for i in res:
        cmd_name = unicodedata.normalize('NFKD', i[0]).encode('ascii', 'ignore')
        cmd_name_list.append(cmd_name)

    # to be changed in the future maybe
    cmd_time_period = find_cmd_period(fn, cmd_name_list, date_time_pattern, start_time)
    # for each command used during the test, find its corresponding running period

    plt.figure(figure_title)

    cmd_num = len(cmd_time_period.keys())
    count = 0
    for cmd in cmd_time_period.keys():
        ax = plt.subplot2grid((3, cmd_num), (0, count), colspan=1)
        ax.axes.get_yaxis().set_visible(True)
        try:
            simplified_cmd_name = thermal_commands_lookup[cmd]
        except:
            simplified_cmd_name = 'unknow'
        xlabel = 'P Temp' + u'\N{DEGREE SIGN}' + ' (' + simplified_cmd_name + ')'
        ax.set_xlabel(xlabel)
        ax.set_ylabel('J Temp' + u'\N{DEGREE SIGN}')
        ax.grid(True)
        plt.plot(cmd_time_period[cmd]['probe_temperature'], cmd_time_period[cmd]['junction_temperature'])
        count += 1

    ax2 = plt.subplot2grid((3, cmd_num), (1, 0), colspan=cmd_num)
    plt.plot(time_stamps, junction_temperature)
    plt.text(avg(time_stamps[-1], time_stamps[-1] + 5), avg(junction_temperature[-1] + 15, junction_temperature[-1]),
             'Junction Temperature', \
             horizontalalignment='center', verticalalignment='center')
    plt.plot(time_stamps, probe_temperature)

    plt.text(avg(time_stamps[-1], time_stamps[-1] + 5), avg(probe_temperature[-1], probe_temperature[-1] + 15),
             'Probe Temperature',
             horizontalalignment='center', verticalalignment='center')

    ax2.set_xlabel('time (s)')
    ax2.set_ylabel('Temperature' + u'\N{DEGREE SIGN}')
    ax2.grid(True)

    ax3 = plt.subplot2grid((3, cmd_num), (2, 0), colspan=cmd_num, sharex=ax2)
    ax3.set_ylabel('A')
    ax3.set_xlabel('time (s)')
    ax3.grid(True)
    plt.plot(time_stamps, current)
    for each_cmd in cmd_time_period.keys():
        try:
            label_name = thermal_commands_lookup[each_cmd]
        except:
            label_name = 'unknow command'
        plt.plot(cmd_time_period[each_cmd]['time'], cmd_time_period[each_cmd]['current'], label=label_name)
    # always the last step since the whole process halt there
    plt.legend(loc=2, shadow=True, prop={'size': 9}, borderaxespad=0.)
    plt.show()


# plot main
def plot(fn, plot_type, figure_num):
    if plot_type == PLOT_TYPE_EVENT_TEMP:
        figure_title = ' Figure ' + figure_num + ' Event and Temperature Plot'
        plot_FCC(fn, figure_title)
    elif plot_type == PLOT_TYPE_TEMP_RESULTS:
        figure_title = ' Figure ' + figure_num + ' Thermal Results Plot'
        plot_thermal(fn, figure_title)


if __name__ == '__main__':
    fn = sys.argv[1]
    plot_type = sys.argv[2]
    figure_num = sys.argv[3]
    plot(fn, plot_type, figure_num)
