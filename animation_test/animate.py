#!/usr/bin/python
import io
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from multiprocessing import Process
import logging
import time
import StringIO
from collections import OrderedDict


class KEY:

    TIMESTAMP = 'timestamp'
    TEMP_LOAD = 'temp_load'
    TEMP_SKIN = 'temp_skin'
    FREQ = 'freq'
    DELTA_POWER = 'delta_power'
    CONTEXT_KEYS = OrderedDict([(TIMESTAMP, 'Timestamp'),
                                (FREQ, 'Frequency'),
                                (TEMP_LOAD, 'Load Temperature'),
                                (TEMP_SKIN, 'Skin Temperature'),
                                (DELTA_POWER, 'Delta Power')])

    LOAD_ = 'load_'
    ANT_  = 'ant_'

    PWR_MEDIAN = 'pwr_median'
    PWR_MEAN = 'pwr_mean'
    PWR_MEDMEAN = 'pwr_medmean'
    PWR_MIN = 'pwr_min'
    PWR_MAX = 'pwr_max'
    PWR_STDDEV = 'pwr_stddev'
    PWR_IQR = 'pwr_iqr'
    POWER_KEYS = OrderedDict([(PWR_MEDIAN, 'Median'),
                              (PWR_MEAN, 'Mean'),
                              (PWR_MEDMEAN, 'Med-Mean'),
                              (PWR_MIN, 'Min'),
                              (PWR_MAX, 'Max'),
                              (PWR_STDDEV, 'StdDev'),
                              (PWR_IQR, 'IQR')])

def remove_column(a, name):
    names = list(a.dtype.names)
    names.remove(name)
    return a[names]

class DataPlotter(object):

    def __init__(self, csv_handle, window=None, timeout=0):

        self.csv_handle = csv_handle
        # Instantiate an ndarray for syntax highlighting purposes.
        self.data = None

        # Number of seconds to display on the graph
        self.window = window

        self.timeout = timeout

    def show(self):
        self.fig, self.axarr = plt.subplots(2, 1)
        self.fig.set_tight_layout(True)
        self.ani = animation.FuncAnimation(self.fig, self.animate, interval=200, blit=False)
        plt.show()

    def animate(self, i):
        if not self.update_lists(window=self.window):
            logging.debug("No new data.")
            return

        # TEMPERATURE PLOTS
        self.axarr[0].set_title("Skin Temperature")
        plt.setp(self.axarr[0].xaxis.get_majorticklabels(), visible=False)
        self.axarr[0].plot(self.data[KEY.TIMESTAMP],
                             self.data[KEY.TEMP_SKIN])

        self.axarr[1].set_title("Load Temperature")
        self.axarr[1].plot(self.data[KEY.TIMESTAMP],
                             self.data[KEY.TEMP_LOAD])

    def update_lists(self, window=None):
        # Read remainder of file into string.
        new_lines_str = self.csv_handle.read()
        has_new_data = (new_lines_str.strip() != '')
        if has_new_data:
            new_lines = StringIO.StringIO(new_lines_str)

            if self.data is None:
                self.data = np.genfromtxt(fname=new_lines, dtype=float, delimiter=',', names=True)
            else:
                new_data = np.genfromtxt(fname=new_lines, dtype=self.data.dtype, delimiter=',')
                if self.data.size == 0:
                    self.data = new_data
                else:
                    self.data = np.hstack((self.data, new_data))

            # We must wait until the self.data array has 2 rows before we can
            # use it or else we get indexing errors: "IndexError: 0-d arrays
            # can't be indexed". (This smells like a Numpy bug, since an array
            # with one row is definitely not a 0-d array.)
            # (Who cares if we can't make a plot containing only one data point?)
            has_new_data = self.data.size > 1

            # Now take the most recent time, subtract <window> seconds, and use that to find
            # the start index for our array. Then we truncate everything that falls
            # outside of that window.
            if window and has_new_data:
                # Truncate the old out-of-window stuff from our array.
                begin_time = self.data[KEY.TIMESTAMP][-1] - window
                self.data = self.data[self.data[KEY.TIMESTAMP] > begin_time]

        return has_new_data



if __name__ == "__main__":
    plot_csv_handle = io.open("fake_data.csv", "r")
    plotter = DataPlotter(plot_csv_handle)
    plotter_proc = Process(target=plotter.show, name="Live Data Plotter")
    plotter_proc.daemon = True
    plotter_proc.start()
    while True:
        time.sleep(0.5)