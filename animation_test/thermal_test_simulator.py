#!/usr/bin/python

import csv
import subprocess
import os, time, sys, signal

subproc = None
from RT_plotter import pipe_name
from RT_plotter import msg_pattern

def receive_signal(signum, stack):
    global subproc
    print 'Quiting'
    subproc.kill()  # kill the subprocess
    exit()


def make_msg(row):
    msg = ''
    for i in msg_pattern:
        msg += (row[i] + ' ')
    return msg


if __name__ == "__main__":
    fn = '../sample_data/thermal_test_results/passive_starthigh.csv'
    signal.signal(signal.SIGINT, receive_signal)
    subproc = subprocess.Popen(['./RT_plotter.py'])
    with open(fn, 'rb') as fin:  # `with` statement available in 2.5+
        dr = csv.DictReader(fin)  # comma is default delimiter
        for row in dr:
            time.sleep(0.5)
            J_temp = row['junction_temperature']
            P_temp = row['probe_temperature']
            # msg = J_temp + ' ' + P_temp
            msg = make_msg(row)
            sudo_pipe = open(pipe_name, 'w')
            sudo_pipe.write(msg)
            sudo_pipe.close()
            os.kill(subproc.pid, signal.SIGINT)
        # print 'Waiting here'