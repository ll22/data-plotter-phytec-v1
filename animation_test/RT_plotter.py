#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random, signal
import os, time, sys
import time
pipe_name = 'sudo_pipe'
msg_pattern = ['probe_temperature', 'chamber_temperature', 'junction_temperature', 'current']

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_ylim(0, 20)

count = 0
xar = [0, 0]
y_J = [0, 90]
y_P = [0, 90]

x_P = []
x_J = []
y_C = [0, 90]
y_current = [0, 0.4]
received_J_data = 30
received_P_data = 30
received_C_data = 30
received_current_data = 0.2
# ax1.plot(xar,y_J)
ax1 = plt.subplot2grid((3, 1), (0, 0), colspan=1)
ax2 = plt.subplot2grid((3, 1), (1, 0), colspan=1)
ax3 = plt.subplot2grid((3, 1), (2, 0), colspan=1)


def receive_signal(signum, stack):
    global received_J_data
    global received_P_data
    global received_C_data
    global received_current_data
    sudo_pipe = open(pipe_name, 'r')
    rec = sudo_pipe.readline()
    temps = rec.split(' ')
    received_J_data = float(temps[0])
    received_C_data = float(temps[1])
    received_P_data = float(temps[2])
    received_current_data = float(temps[3])

    sudo_pipe.close()
    print 'signum: ', signum
    print 'Received J: ', received_J_data
    print 'Received C: ', received_C_data
    print 'Received P: ', received_P_data
    print 'Received current: ', received_current_data

def animate(i):
    global count
    global xar
    global y_J
    global y_P
    global y_C
    global x_J
    global x_P
    global received_J_data
    global received_P_data
    global received_C_data
    # delta_y = random.randrange(10, 14)
    xar.append(count)
    # yar.append(delta_y)
    y_J.append(received_J_data)
    x_J.append(received_J_data)
    x_P.append(received_P_data)
    y_P.append(received_P_data)
    y_C.append(received_C_data)
    y_current.append(received_current_data)
    count += 1
    #plt.legend(loc=1, shadow=True, prop={'size': 9}, borderaxespad=0.)
    ax2.clear()
    ax2.grid(True)
    ax2.plot(xar, y_current,  label='current')
    ax2.set_ylabel('A')
    ax2.set_xlabel('time (s)')

    ax3.clear()
    ax3.grid(True)
    ax3.plot(x_P, x_J)
    ax3.set_ylabel('Junction Temperature' + u'\N{DEGREE SIGN}')
    ax3.set_xlabel('Probe Temperature' + u'\N{DEGREE SIGN}')
    plt.xlim(0, 90)
    plt.ylim(0, 90)

    ax1.clear()
    ax1.grid(True)
    ax1.set_ylabel('Temperature' + u'\N{DEGREE SIGN}')
    ax1.plot(xar, y_J,  label='Junction temp')
    ax1.plot(xar, y_P,  label='Probe temp')
    ax1.plot(xar, y_C,  label='Chamber temp')
    ax1.legend(loc=3, shadow=True, prop={'size': 12}, borderaxespad=0.)
    #plt.legend(loc=2, shadow=True, prop={'size': 9}, borderaxespad=0.)
    # plt.legend()


if __name__ == "__main__":
    signal.signal(signal.SIGINT, receive_signal)
    ani = animation.FuncAnimation(fig, animate, interval=10)
    plt.show()

    #while True:
    #    print 'Waiting...'
    #    time.sleep(3)